---
title: Présentation du Kit Markdown
---

# Kit_markdown

Kit nécessaire pour la production de Markdown et PlantUML, en utilisant le logiciel [Pandoc](https://pandoc.org/).

  cf. [MUL_Markdown.md](MUL_Markdown.md) pour plus de détails.

## Utilisation du kit sous Windows

### Installation

> A faire !

### Génération des documents

On peut générer les documents dans le répertoire _out_ dans les format **HTML**, **DOCX**, et **PDF** (via latex), à l'aide de la commande suivante :

```bash
cd Kit_markdown-master
make_all.bat
dir out
```

### Configuration de *Visual Code Studio* sous Windows

Il faut installer l'extension suivante dans Visual Studio Code : [vscode-pandoc](https://marketplace.visualstudio.com/items?itemName=DougFinke.vscode-pandoc), puis configurer cette extension avec les lignes suivantes :

```json
{
    "pandoc.docxOptString": "--from markdown+mmd_title_block+table_captions+multiline_tables+grid_tables+implicit_figures+task_lists+lists_without_preceding_blankline+tex_math_dollars --lua-filter=c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/diagram-generator.lua --metadata=plantumlPath:\"c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/plantuml.jar\" --metadata=dotPath:\"c:/Program Files (x86)/Graphviz2.38/bin/dot.exe\" --self-contained --standalone --reference-doc=c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/MDL_Tech-Med.docm --table-of-contents",
    "pandoc.htmlOptString": "--from markdown+mmd_title_block+table_captions+multiline_tables+grid_tables+implicit_figures+task_lists+lists_without_preceding_blankline+tex_math_dollars --lua-filter=c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/diagram-generator.lua --metadata=plantumlPath:\"c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/plantuml.jar\" --metadata=dotPath:\"c:/Program Files (x86)/Graphviz2.38/bin/dot.exe\" --self-contained --standalone --table-of-contents --css=c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/style.css --number-sections --mathjax",
    "pandoc.pdfOptString": "--from markdown+mmd_title_block+table_captions+multiline_tables+grid_tables+implicit_figures+task_lists+lists_without_preceding_blankline+tex_math_dollars --lua-filter=c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/diagram-generator.lua --metadata=plantumlPath:\"c:/Users/baptiste.pierard/Documents/Perso/Kit_markdown_from_GitHub/ref/plantuml.jar\" --metadata=dotPath:\"c:/Program Files (x86)/Graphviz2.38/bin/dot.exe\" --self-contained --toc --top-level-division=chapter --shift-heading-level-by=1 --number-sections --variable mainfont=\"Liberation Serif\" --variable sansfont=\"Liberation Sans\" --variable monofont=\"Liberation Mono\" --variable fontsize=10pt --variable documentclass=book -V geometry:margin=2cm"
}

```
:warning: Les chemins "en dur" des lignes suivantes doivent bien évidemment être ajustés !

Il est possible d'obtenir automatiquement les lignes de configuration en lançant la commande suivante :
```batch
make_all.bat vsc
```

On peux alors générer les documents à l'aide des raccourcis suivants :
- `<Ctrl>+<k>` +  `p` + `h` : document HTML
- `<Ctrl>+<k>` +  `p` + `d` : document Microsoft Word (**docx**)
- `<Ctrl>+<k>` +  `p` + `p` : document PDF (via *Latex*)

## Utilisation du kit sous Linux

### Installation

C'est évidemment beaucoup plus simple que sous Windows ;-) !

```bash
sudo apt update
sudo apt install pandoc graphviz texlive-latex-base texlive-full libjs-mathjax
git clone git@github.com:CyberPQ/Kit_markdown.git
```

Si aucun développement n'est prévu, on peut remplacer le `git clone ...` ci-dessus par les commandes suivantes :
```bash
wget https://gitlab.com/baptiste.pierard/Kit_markdown/-/archive/master/Kit_markdown-master.zip
unzip master.zip
```

### Génération des documents 

On peut générer les documents dans le répertoire _out_ dans les format **HTML**, **DOCX**, et **PDF** (via latex), à l'aide de la commande suivante :

```bash
cd Kit_markdown
make all
ls -1 out
```

### Configuration de *Visual Code Studio* sous Linux

```json
{
    "pandoc.docxOptString": "-f markdown_github+mmd_title_block+table_captions+multiline_tables+grid_tables+implicit_figures+inline_notes+yaml_metadata_block --reference-doc=/home/bpr/Documents/Kit_markdown_gitlab/ref/MDL_Tech-Med.docm --toc --lua-filter=/home/bpr/Documents/Kit_markdown_gitlab/ref/diagram-generator.lua --metadata=plantumlPath:/home/bpr/Documents/Kit_markdown_gitlab/ref/plantuml.jar",
    "pandoc.pdfOptString": "-f markdown_github+mmd_title_block+table_captions+multiline_tables+grid_tables+implicit_figures+inline_notes+yaml_metadata_block --self-contained --toc --top-level-division=chapter --shift-heading-level-by=1 --number-sections --variable mainfont=\"Liberation Serif\" --variable sansfont=\"Liberation Sans\" --variable monofont=\"Liberation Mono\" --variable fontsize=10pt --variable documentclass=book -V geometry:margin=2cm --lua-filter=/home/bpr/Documents/Kit_markdown_gitlab/ref/diagram-generator.lua --metadata=plantumlPath:/home/bpr/Documents/Kit_markdown_gitlab/ref/plantuml.jar --pdf-engine=xelatex",
    "pandoc.htmlOptString": "-s -t html5 --toc -N -f markdown_github+mmd_title_block+table_captions+multiline_tables+grid_tables+implicit_figures+inline_notes+yaml_metadata_block --css=/home/bpr/Documents/Kit_markdown_gitlab/ref/style.css --lua-filter=/home/bpr/Documents/Kit_markdown_gitlab/ref/diagram-generator.lua --metadata=plantumlPath:/home/bpr/Documents/Kit_markdown_gitlab/ref/plantuml.jar",
}
```

:warning: Les chemins "en dur" des lignes suivantes doivent bien évidemment être ajustés !

On peux alors générer les documents à l'aide des raccourcis suivants :
- `<Ctrl>+<k>` +  `p` + `h` : document HTML
- `<Ctrl>+<k>` +  `p` + `d` : document Microsoft Word (**docx**)
- `<Ctrl>+<k>` +  `p` + `p` : document PDF (via *Latex*)
